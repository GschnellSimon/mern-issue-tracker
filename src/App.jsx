


//regex used to detect dateStrings from the issueList
const dateRegex = new RegExp('^\\d\\d\\d\\d-\\d\\d-\\d\\d');
function jsonDateReviver(key, value){
  if(dateRegex.test(value)) return new Date(value);
  return value;
}

const sampleIssue = {
  status: "New",
  owner: "Pieta",
  title: "Completion date should be optional",
};

class IssueFilter extends React.Component {
  render() {
    return (
      <React.Fragment>
        <h2>This is a placeholder for the issue filter.</h2>
      </React.Fragment>
    );
  }
}

function IssueTable (props) {
  
    const rowStyle = { border: "1px solid silver", padding: 4 };


    const IssueRows = props.issues.map((issue) => (
      <IssueRow key={issue.id} style={rowStyle} issue={issue} />
    ));

    return (
      <>
        <table style={{ borderCollapse: "collapse" }}>
          <thead>
          <th style={rowStyle} >id</th>
          <th style={rowStyle} >staus</th>
          <th style={rowStyle} >owner</th>
          <th style={rowStyle} >effort</th>
          <th style={rowStyle} >created</th>
          <th style={rowStyle} >due</th>
          <th style={rowStyle} >title</th>
          </thead>
          <tbody>{IssueRows}</tbody>
        </table>
      </>
    );
  
}

function IssueRow (props){
    const { style, issue } = props;
  return(
    
   
      <>
        <tr>
          <td style={style}>{issue.id}</td>
          <td style={style}>{issue.status}</td>
          <td style={style}>{issue.owner}</td>
          <td style={style}>{issue.effort}</td>
          <td style={style}>{issue.created? issue.created.toDateString():''}</td>
          <td style={style}>{issue.due? issue.due.toDateString():'' }</td>
          <td style={style}>{issue.title}</td>
        </tr>
      </>
    )
  }

class IssueAdd extends React.Component {
    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }
  handleSubmit(event){
    event.preventDefault();
    const form = document.forms.issueAdd;
    let title = form.title.value.replace("\"","'");
    let owner = form.owner.value;
    const issue = {
        status: "New", title , owner, due:new Date()
    }
    this.props.add(issue);
    title='';
    owner='';
  }

  render() {
    return (
      <>
        <form style={{padding:20}} name="issueAdd" onSubmit={this.handleSubmit}>
          <input style={{marginLeft:10, marginRight: 10}} type="text" name="owner" placeholder="Owner" />
          <input style={{marginLeft:10, marginRight: 10}} type="text" name="title" placeholder="Title" />
          <button style={{background:"lightgreen"}} >Add</button>
        </form>
      </>
    );
  }
}

class IssueList extends React.Component {
  constructor() {
    super();
    this.state = { issues: [] };
    this.createIssue = this.createIssue.bind(this);
  }

   async loadData() {
    const query = `query{
      issueList{
        id title status owner
        created effort due
      }
    }`
    const response =  await fetch("/graphql",{
      method: 'POST',
      headers: {'Content-Type':'application/json'},
      body:JSON.stringify({query})
    });
    const body = await response.text();
    const result = JSON.parse(body,jsonDateReviver);
    this.setState({issues: result.data.issueList})
  }

  componentDidMount() {
    this.loadData();
  }

  async createIssue  (issue)  {
    const query=`mutation{
      issueAdd(issue:{
        title:"${issue.title}",
        owner:"${issue.owner}",
        due:"${issue.due.toISOString()}"
      }){
        id
      }
    }`;

    const response = await fetch("/graphql",{
      method:'POST',
      headers:{'Content-Type':'application/json'},
      body : JSON.stringify({query})
    })

    this.loadData();
  }

  render() {
    return (
      <>
        <IssueFilter />
        <hr />
        <IssueTable issues={this.state.issues} />
        <hr />
        <IssueAdd add={this.createIssue} />
      </>
    );
  }
}
const element = <IssueList />;

ReactDOM.render(element, document.getElementById("contents"));
