const express = require("express");
const { GraphQLScalarType, Kind, parseValue } = require('graphql');
const { ApolloServer } = require("apollo-server-express");
const fs = require("fs");
const app = express();

app.use("/", express.static("public"));

app.listen(3000, () => {
  console.log("server started");
});

//graphql server
const GraphQlDate = new GraphQLScalarType({
    name: 'GraphQlDate',
    description: 'A Date() type in Graphql as a scalar',
    serialize(value){
        return value.toISOString();
    },
    parseLiteral(ast){
      return (ast.kind == Kind.STRING)?new Date(ast.value):undefined;
    },
    parseValue(value){
      return new Date(value);
    }
})
let aboutMessage = "Issue Tracker API v1.0";
const issuesDB = [
  {
    id: 1,
    status: "New",
    owner: "Ravan",
    effort: 5,
    created: new Date("2019-01-15"),
    due: undefined,
    title: "Error in console when clicking Add",
  },
  {
    id: 2,
    status: "Assigned",
    owner: "Eddie",
    effort: 14,
    created: new Date("2019-01-16"),
    due: new Date("2019-02-01"),
    title: "Missing bottom border on panel",
  },
];
function setAboutMessage(_, { message }) {
  return (aboutMessage = message);
}
function issueAdd(_, { issue }) {
  const newIssue={
    id: issuesDB.length+1,
    status:issue.status?issue.status:'New',
    title:issue.title,
    owner:issue.owner?issue.owner:'',
    effort:issue.effort?issue.effort:undefined,
    due:issue.due?issue.due:undefined,
    created: new Date(),
  }
  issuesDB.push(newIssue);
  return newIssue;
}
function issueList() {
    return issuesDB;
  }

const resolvers = {
  Query: {
    about: () => aboutMessage,
    issueList
  },
  Mutation: {
    setAboutMessage,
    issueAdd
  },
  GraphQlDate
};

const server = new ApolloServer({
  typeDefs: fs.readFileSync("./server/schema.graphql", "utf-8"),
  resolvers,
});

server.applyMiddleware({ app, path: "/graphql" });
